"""Calculate the average seperation between a molecule and a surface of charges
separated by the XY plane"""

import dist_calc
import file_parser

# Read QChem output into coord_file
LINELIST = file_parser.file2list()

COORD_BLOCK = file_parser.coordblock(LINELIST)
SANDWICH_BLOCK_1, SANDWICH_BLOCK_2 = file_parser.sandwichblock(LINELIST)

SUBSTRATE = []
SURFACE_1 = []
SURFACE_2 = []

# Create new lists containing only z-coordinates
for line in COORD_BLOCK:
    SUBSTRATE.append(float(line[4]))
for line in SANDWICH_BLOCK_1:
    SURFACE_1.append(float(line[2]))
for line in SANDWICH_BLOCK_2:
    SURFACE_2.append(float(line[2]))

# Take average of both z-coordinate lists and print the absoulte differnce
DISTANCE_1 = dist_calc.calc_distance(SUBSTRATE, SURFACE_1)
DISTANCE_2 = dist_calc.calc_distance(SUBSTRATE, SURFACE_2)
print('Seperation between first surface and substrate = ', DISTANCE_1)
print('Seperation between second surface and substrate = ', DISTANCE_2)
