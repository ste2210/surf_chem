"""Calculate the average seperation between 2 molecules that are separated by
the XY plane"""

import sys
from file_parser import FileParser as fp

# Read QChem output into coord_file
LINELIST = fp.file2list()

# Search for block of text containing final geometry coordinates
MOL_ATOMS = int(input('Give number of substrate atoms:\n'))
COORD_BLOCK = fp.coordblock(LINELIST)

# Create seperate lists for surface and substrate coordinates
XYZ_POS = input('Substrate atoms appear at top of coordinate output? (y/n):\n')
if XYZ_POS == "y":
    SUBSTRATE_LIST = COORD_BLOCK[:MOL_ATOMS-1]
elif XYZ_POS == "n":
    SUBSTRATE_LIST = COORD_BLOCK[-MOL_ATOMS:]
else:
    print('Invalid entry for substrate atom positions')
    sys.exit()

SUBSTRATE = [str(MOL_ATOMS), '', ]

# Cr
for line in SUBSTRATE_LIST:
    atom_label = (str(line[1]))
    x_coord = (str(line[2]))
    y_coord = (str(line[3]))
    z_coord = (str(line[4]))
    SUBSTRATE.append(atom_label+'  '+x_coord+'   '+y_coord+'   '+z_coord)

fp.list2file(SUBSTRATE)
