"""Takes average of x and y, returns the difference"""


def calc_distance(x, y):
    distance = abs((sum(x)/len(x))-(sum(y)/len(y)))
    return distance
