"""Program for generating hexagonal flakes containing up to two different atom
types."""

import file_parser as fp


# User-defined parameters (Bond Length, Atom Types, Flake Size)
atom_1 = input('Specify atom type 1')
atom_2 = input('Specify atom type 2')
bond_length = float(input('Specify bond length of surface atoms'))
flake_size = int(input('How wide is surface? (number of rings must be odd)'))

# Box parameters
index = int((flake_size + 1)/2)
total_atom_number = 6 * (index ** 2)
number_of_rows = 2 * index
max_row_length = number_of_rows

# Vectors
x_vec = (3 ** (1/2)) * bond_length
y_vec = 3/2 * bond_length

coords = []
coords.append(str(total_atom_number) + "\n")

# Flake is formed row by row, with the length of each row contained in a list
# Generate lists of row/space lengths to iterate over
# Length list has the form [n, n+1, ... 2n, 2n-1, ... n+1]
row_length_list = []
space_length_list = []

# Create list of row lengths
for row_length in range(index, max_row_length):
    row_length_list.append(row_length)
for row_length in reversed(range(index + 1, max_row_length + 1)):
    row_length_list.append(row_length)

# Create list of lengths for spaces at the start of each row
for space_length in reversed(range(0, index + 1)):
    space_length_list.append(space_length)
for space_length in range(1, index):
    space_length_list.append(space_length)

reversed_space_length_list = list(reversed(space_length_list))
reversed_row_length_list = list(reversed(row_length_list))

# Create y-coords for a row of each atom type
for row_number in range(0, number_of_rows):
    Y1 = y_vec * row_number
    Y2 = Y1 + ((1/2) * bond_length)

# Create x-coords for atom type 1
    space_length_1 = int(space_length_list[row_number]) * (x_vec / 2)
    row_length_1 = int(row_length_list[row_number])
    for atom_index in range(0, row_length_1):
        X1 = space_length_1 + (x_vec * atom_index)
        coords.append(str(atom_1)+"   "+str(X1)+"   "+str(Y1)+"   0.0000000")

# Create x-coords for atom type 2
    space_length_2 = int(reversed_space_length_list[row_number]) * (x_vec / 2)
    row_length_2 = int(reversed_row_length_list[row_number])
    for atom_index in range(0, row_length_2):
        X2 = space_length_2 + (x_vec * atom_index)
        coords.append(str(atom_2)+"   "+str(X2)+"   "+str(Y2)+"   0.0000000")

# Transform 'coords' list to 'coord' file
fp.list2file(coords)
