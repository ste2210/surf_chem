"""Calculate the average seperation between a molecule and a surface of charges
separated by the XY plane"""

import dist_calc
from file_parser import FileParser as fp

# Read QChem output into coord_file
LINELIST = fp.file2list()

COORD_BLOCK = fp.coordblock(LINELIST)
CHARGE_BLOCK = fp.chargeblock(LINELIST)

SUBSTRATE = []
SURFACE = []

# Create new lists containing only z-coordinates
for line in COORD_BLOCK:
    SUBSTRATE.append(float(line[4]))
for line in CHARGE_BLOCK:
    SURFACE.append(float(line[2]))

# Take average of both z-coordinate lists and print the absoulte differnce
DISTANCE = dist_calc.calc_distance(SUBSTRATE, SURFACE)
print('Seperation between surface and substrate = ', DISTANCE)
