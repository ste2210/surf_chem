import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="surf_chem",
    version="0.0.1",
    author="Stephen Mason",
    author_email="ste221093@gmail.com",
    description="A programme to help analysis of surface-based QChem calculations",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ste2210/surf_chem",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
