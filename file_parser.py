"""Various functions to allow simpler manipulation of QChem .out files"""

import sys


class FileParser():
    def file2list(filename=sys.argv[1]):
        """Convert user-specified file into a list. Each line in the file
        becomes an element in the list"""

        try:
            with open(filename, 'rt') as file:
                linelist = file.readlines()
                file.close()
            return linelist
        except FileNotFoundError:
            print(filename, 'cannot be found')
            sys.exit()

    def list2file(file_list):
        """Convert list to a file, where each list item is given a new line"""

        filename = input('save file as: ')
        file = open(filename, 'w')

        for item in file_list:
            file.write(item+'\n')
        file.close()

    def coordblock(linelist):
        """Create coord_block, a list of the cartesian coordinates found for a
        final converged structure"""

        start = [i for i, line in enumerate(
                linelist) if 'OPTIMIZATION C' in line][0]
        end = [i for i, line in enumerate(
                linelist) if 'Z-matrix Print:' in line][0]
        coord_block = linelist[start+5:end-1]
        coord_block = [line.split() for line in coord_block]
        return coord_block

    def chargeblock(linelist):
        """Create charge_block, a list of the cartesian coordinates for the
        external charges specified in the QChem input file"""

        ncharge = int(input('How many charges are present?\n'))
        start = [i for i, line in enumerate(
                linelist) if 'external_' in line][0]
        end = start + ncharge + 1
        charge_block = linelist[start+1:end]
        charge_block = [line.split() for line in charge_block]
        return charge_block

    def sandwichblock(linelist):
        """Create charge_block, a list of the cartesian coordinates for the
        external charges specified in the QChem input file"""

        ncharge = int(input('How many charges are present?\n'))
        start = [i for i, line in enumerate(
                linelist) if 'external_' in line][0]
        layer_1_start = int(start + 1)
        layer_1_end = int(layer_1_start + (ncharge / 2))
        layer_2_start = int(layer_1_end + 1)
        layer_2_end = int(start + ncharge + 1)
        sandwich_block_1 = linelist[layer_1_start:layer_1_end]
        sandwich_block_1 = [line.split() for line in sandwich_block_1]
        sandwich_block_2 = linelist[layer_2_start:layer_2_end]
        sandwich_block_2 = [line.split() for line in sandwich_block_2]
        return sandwich_block_1, sandwich_block_2
