"""Calculate the average seperation between 2 molecules that are separated by
the XY plane"""

import sys
import dist_calc
from file_parser import FileParser as fp

# Read QChem output into coord_file
LINELIST = fp.file2list()

# Search for block of text containing final geometry coordinates
MOL_ATOMS = int(input('Give number of substrate atoms:\n'))
COORD_BLOCK = fp.coordblock(LINELIST)

# Create seperate lists for surface and substrate coordinates
XYZ_POS = input('Substrate atoms appear at top of coordinate output? (y/n):\n')
if XYZ_POS == "y":
    SUBSTRATE_LIST = COORD_BLOCK[:MOL_ATOMS-1]
    SURFACE_LIST = COORD_BLOCK[MOL_ATOMS:]
elif XYZ_POS == "n":
    SURFACE_LIST = COORD_BLOCK[:-MOL_ATOMS]
    SUBSTRATE_LIST = COORD_BLOCK[-MOL_ATOMS:]
else:
    print('Invalid entry for substrate atom positions')
    sys.exit()

SUBSTRATE = []
SURFACE = []

# Create new lists containing only z-coordinates
for line in SUBSTRATE_LIST:
    SUBSTRATE.append(float(line[4]))
for line in SURFACE_LIST:
    SURFACE.append(float(line[4]))

# Take average of both z-coordinate lists and print the absoulte differnce
DISTANCE = dist_calc.calc_distance(SUBSTRATE, SURFACE)
print('Seperation between surface and substrate = ', DISTANCE)
